﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Reflection;
using System.Drawing.Imaging;
using System.Threading;
using OrgerLab.Utils;
using OrgerLab.Cameras.Hamamatsu;
using System.Runtime.InteropServices;

namespace OrgerLab.Acquisition.Hamamatsu
{
    public partial class CamControl : Form
    {
        int[] frameInfo;
        byte[] img16bpp;
        int buff_read_idx = -1;
        int cam_framesPreAcq = 0;
        int acq_lag = 0;
        int cam_totalFrames = 0;
        int acq_idx = 0;
        OrcaFlash4 cam;
        BackgroundWorker acqThread;
        BackgroundWorker displayThread;

        public CamControl()
        {
            InitializeComponent();

            // Configure camera
            int nCams = OrgerLab.Cameras.Hamamatsu.Hamamatsu.Init();
            cam = new OrcaFlash4(0);
            propertyGrid_cam.SelectedObject = cam;


            displayThread = new BackgroundWorker();
            byte[] imgDisp;
            displayThread.DoWork += new DoWorkEventHandler(delegate(object sender, DoWorkEventArgs e)
            {

                Console.WriteLine("Display: thread started.");
                while (!displayThread.CancellationPending)
                {
                    //Console.WriteLine("Display: checking new image...");
                    if (frameAcquired.WaitOne(500))
                    {
                        
                        imgDisp = new byte[imgCopy.Length];
                        unsafe
                        {
                            fixed (byte* imgPtr = imgCopy)
                            {
                                Marshal.Copy((IntPtr)imgPtr, imgDisp, 0, imgCopy.Length);
                            }
                        }
                        Bitmap bmp = Bmp_from_byte16(imgDisp, cam.DataSizeEx.cx, cam.DataSizeEx.cy, true);
                        pictureBox.BeginInvoke(new Action(() =>
                        {
                            pictureBox.Image = bmp;
                            //Console.WriteLine("Display: displayed new frame.");
                            status_dispFrame.Text = framesAcquired.ToString();
                        }));
                    }
                }
            });
            displayThread.RunWorkerAsync();


            OpenCamera();
                                                       
        }

        byte[] imgCopy;
        AutoResetEvent frameAcquired = new AutoResetEvent(false);
        static long framesAcquired = 0;

        /// <summary>
        /// Start acquisition. Loop: getFrameInfo + getFrame (dcam_wait + dcam_lockdata + Marshal.Copy + dcam_unlockdata)
        /// </summary>
        /// 
        private void StartAcquisition()
        {
            propertyGrid_cam.Enabled = false;

            if (acqThread != null && acqThread.IsBusy)
            {
                acqThread.CancelAsync();
            }

            //
  

            //
            acqThread = new BackgroundWorker();
            acqThread.WorkerReportsProgress = true;
            acqThread.WorkerSupportsCancellation = true;
            //m_grabThread.ProgressChanged += new ProgressChangedEventHandler(UpdateUI);
            acqThread.DoWork += new DoWorkEventHandler(delegate(object sender, DoWorkEventArgs e) {

                // Precapture (To Stable state) 
                if (!cam.Precapture())
                {
                    Console.WriteLine("Could not precapture.");
                    Console.ReadLine();
                    return;
                }

                uint imgSize = cam.DataSize; 

                // Allocate memory (To Ready state)
                int bufferSizeInFrames = cam.BufferSizeRequest;
                propertyGrid_cam.BeginInvoke(new Action(() => { propertyGrid_cam.Refresh(); }));
                if (!cam.Allocate(bufferSizeInFrames))
                {
                    Console.WriteLine("Could not allocate .");
                    Console.ReadLine();
                    return;
                }



                // Start capture (to Busy state)
                if (!cam.capture())
                {
                    Console.WriteLine("Could not start capture.");
                    Console.ReadLine();
                    return;
                }


                // Start grab            
                int framesToAcq = cam.FramesToAcquire;               
                int idxRequested = 0;
                int[] frameInfo;
                Console.WriteLine("Acquiring " + framesToAcq + " frames...");
                Stopwatch watchAcq = new Stopwatch();
                Stopwatch watchDisp = new Stopwatch();
                int grabCount;
                framesAcquired = 0;
                string log;
                bool displayReady = true;
                for (grabCount = 0; framesAcquired < framesToAcq && !acqThread.CancellationPending; grabCount++)
                {
                    // Get acq. info
                    frameInfo = cam.getFrameInfo();
                    //Console.WriteLine("FrameInfo: total: " + frameInfo[0] + " lastIndex: " + frameInfo[1]);

                    // First id to ask
                    if (frameInfo[1] < 0)
                        continue;
                    else if (framesAcquired == 0)
                    {
                        idxRequested = frameInfo[1];
                    }

                    // Get frame
                    imgCopy = cam.getFrame(idxRequested % bufferSizeInFrames);
                    frameAcquired.Set();

                    if (imgCopy == null)
                    {
                        //Console.WriteLine("Failed to grab frame id: " + idxRequested);
                        break;
                    }
                    else
                    {
                        if (framesAcquired == 0)
                        {
                            watchAcq.Start();
                            watchDisp.Start();
                        }

                        ++framesAcquired;
                        log = "Frame: " + framesAcquired +"/"+framesToAcq + " (" + watchAcq.ElapsedMilliseconds + "ms)";
                        //Console.WriteLine(log);
                        ++idxRequested;
                        
                    }

                    // Displays
                    if ( displayReady && imgCopy != null) //watchDisp.ElapsedMilliseconds > 50
                    {
                        // Display status
                        statusStrip1.BeginInvoke(new Action(() => { 
                            status.Text = log;
                        }));

                        

                        // Display image
                        watchDisp.Restart();
                        //pictureBox.Image = ImgProc.Bmp_from_byte16(imgCopy, imgSize.cx, imgSize.cy, true);
                    }
                }

                // Display at end
                log = "Frame: " + framesAcquired + "/" + framesToAcq + " (" + watchAcq.ElapsedMilliseconds + "ms)";
                statusStrip1.BeginInvoke(new Action(() => { status.Text = log; }));

                Console.WriteLine("Acquisition ended. Acquired " + framesAcquired + " frames in " + watchAcq.ElapsedMilliseconds + "ms.");



                // Stop capture (to Ready state)
                if (!cam.Idle())
                {
                    Console.WriteLine("Could not stop capture.");
                    Console.ReadLine();
                    return;
                }



                // Release memory (to Stable state)
                if (!cam.FreeCamBuffer())
                {
                    Console.WriteLine("Could not release camera buffer.");
                    Console.ReadLine();
                    return;
                }



                propertyGrid_cam.BeginInvoke(new Action(()=> {propertyGrid_cam.Enabled = true;} ));
            });            
            acqThread.RunWorkerAsync();

        }

        public static Bitmap Bmp_from_byte16(byte[] imgByte16pp, int width, int height, bool processing, ushort max = 65535, ushort min = 0, bool autocontrast = true)
        {
            Bitmap bmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            BitmapData bmd = bmp.LockBits(new Rectangle(0, 0, width, height),
                System.Drawing.Imaging.ImageLockMode.ReadOnly, bmp.PixelFormat);


            if (processing && autocontrast)
            {
                ushort[] minMax = ImgProc.MinMax(imgByte16pp); //takes 38ms for 8Mbytes (2048x2048 image) !!!
                max = minMax[1];
                min = minMax[0];
            }

            unsafe
            {
                int pixelSize = 3;
                int i, j, j1, i1;
                byte b;
                double sVal;
                double lPixval;
                double lPixval_offset;
                double lPixval_range;

                //autocontrast
                //double max = (Math.Pow(2, 16) - 1);
                //double fact = (max / 2) / imgAvg;

                //timer.Restart();

                // Takes 1080ms for 8Mbytes (2048x2048 image) !!!
                // Takes 67ms for 500Kbytes (512x512 image)
                for (i = 0; i < bmp.Height; ++i)
                {
                    byte* row = (byte*)bmd.Scan0 + (i * bmd.Stride);
                    i1 = i * bmp.Width;

                    for (j = 0; j < bmp.Width; ++j)
                    {
                        //convert from byte to double
                        sVal = BitConverter.ToUInt16(imgByte16pp, 2 * (i1 + j));

                        if (processing)
                        {
                            //adjustments -> takes virtualy 0ms                        
                            lPixval_offset = (sVal - min);
                            if (lPixval_offset < 0)
                                lPixval_offset = 0;

                            lPixval_range = (max - min);
                            if (lPixval_range < 1)
                                lPixval_range = 1;

                            lPixval = lPixval_offset / lPixval_range * 255.0; // Convert to a 255 value range
                            if (lPixval > 255) lPixval = 255;
                            if (lPixval < 0) lPixval = 0;
                        }
                        else
                            lPixval = sVal / 65535 * 255.0;

                        //write to BMP
                        b = (byte)(lPixval);
                        j1 = j * pixelSize;
                        row[j1] = b;            // Red
                        row[j1 + 1] = b;        // Green
                        row[j1 + 2] = b;        // Blue
                    }
                }
            }
            //Console.WriteLine("TIMER: {0}ms", timer.ElapsedMilliseconds);

            bmp.UnlockBits(bmd);
            return bmp;
        }


        private void StopAcquisition()
        {
            if (acqThread!=null)
                acqThread.CancelAsync();
            
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            StartAcquisition();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StopAcquisition();
        }

        private void CamControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                StopAcquisition();
                cam.disconnect();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenCamera();
        }

        private void OpenCamera()
        {
            Console.WriteLine("Opening camera...");
            if (cam.Open() != IntPtr.Zero)
            {
                propertyGrid_cam.Enabled = true;
                Console.WriteLine("Camera opened.");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            propertyGrid_cam.Enabled = false;
            if (!cam.disconnect())
                propertyGrid_cam.Enabled = true;

            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            cam.LoadFromFile(AppDomain.CurrentDomain.BaseDirectory + @"xmls\config.xml");
            propertyGrid_cam.Refresh();
        }

    }
}
