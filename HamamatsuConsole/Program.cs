﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using OrgerLab.Cameras.Hamamatsu;

namespace HamamatsuConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            OrcaFlash4 cam;


            // Init (get available cameras)
            int nCams = Hamamatsu.Init();
            if (nCams < 1)
            {
                Console.WriteLine("Could not initiate OrcaFlash.");
                Console.ReadLine();
                return;
            }
            else
            {
                for (int i = 0; i < nCams; ++i)
                {
                    cam = new OrcaFlash4(i);
                    Console.WriteLine(cam.CamModel);
                }
            }



            // Open camera
            int camIndex = 0;
            cam = new OrcaFlash4(camIndex);
            Console.WriteLine(cam.CamModel);
            if (cam.Open() == IntPtr.Zero)
            {
                Console.WriteLine("Could not open camera with index: " + camIndex+".");
                Console.ReadLine();
                return;
            }



            // Set-get       
            cam.Dtype = DCAM_DATATYPE.DCAM_DATATYPE_UINT16;
            cam.Btype = DCAM_BITSTYPE.DCAM_BITSTYPE_INDEX8;
            cam.Binning = DCAM_BINNING.DCAM_BINNING1;
            cam.Exposure = 0.05;
            cam.TrigMode = DCAM_TRIGGERMODE.DCAM_TRIGMODE_INTERNAL;
            cam.InPolarity = DCAM_TRIGGERPOLARITY.DCAM_TRIGPOL_NEGATIVE;
            cam.OutTrigPol = DCAM_IDPROP_OUTPUTTRIGGER_POLARITY.DCAMPROP_OUTPUTTRIGGER_POLARITY__POSITIVE;
            cam.OutTrigDelay = 0;
            cam.OutTrigPeriod = 0.001;
            cam.OutTrigKind = DCAM_IDPROP_OUTPUTTRIGGER_KIND.DCAMPROP_OUTPUTTRIGGER_KIND__PROGRAMABLE;
            cam.OutTrigSource = DCAM_IDPROP_OUTPUTTRIGGER_SOURCE.DCAMPROP_OUTPUTTRIGGER_SOURCE__READOUTEND;
            cam.SensorMode = _DCAM_IDPROP_SENSORMODE.DCAMPROP_SENSORMODE__AREA;
            cam.CaptureMode = DCAM_CAPTUREMODE.DCAM_CAPTUREMODE_SEQUENCE;
            cam.Subarray_mode = (double)MODE.DCAMPROP_MODE__ON;
            cam.Hsize = 2048;
            cam.Vsize = 2048;
            cam.OffsetX = 0;
            cam.OffsetY = 0;


            // Precapture (To Stable state) 
            if (!cam.Precapture())
            {
                Console.WriteLine("Could not precapture.");
                Console.ReadLine();
                return;
            }
            


            // Allocate memory (To Ready state)
            int bufferSizeInFrames = 100;
            if (!cam.Allocate(bufferSizeInFrames))
            {
                Console.WriteLine("Could not allocate .");
                Console.ReadLine();
                return;
            }
            


            // Start capture (to Busy state)
            if (!cam.capture())
            {
                Console.WriteLine("Could not start capture.");
                Console.ReadLine();
                return;
            }



            // Start grab
            int framesToAcq = 1000;
            byte[] imgCopy;
            int idxRequested;
            Console.WriteLine("Acquiring " + framesToAcq+" frames...");
            Stopwatch watchAcq = new Stopwatch();
            for (int i = 0; i < framesToAcq; )
            {
                int[] frameInfo = cam.getFrameInfo();
                Console.WriteLine("FrameInfo: total: " + frameInfo[0] + " lastIndex: " + frameInfo[1]);
                if (frameInfo[1] < 0)
                    continue;

                idxRequested = frameInfo[1];
                imgCopy = cam.getFrame(idxRequested);
                if (imgCopy == null)
                {
                    Console.WriteLine("Failed to grab frame id: " + idxRequested);                    
                }
                else
                {
                    if (i == 0)
                    {
                        watchAcq.Start();
                    }

                    Console.WriteLine("FramesGrabbed: " + i + " lastGrabedIdx: " + idxRequested + " time: " + watchAcq.ElapsedMilliseconds+"ms.");
                    ++i;
                }                
            }
            Console.WriteLine("Acquisition ended.");





            // Stop capture (to Ready state)
            if (!cam.Idle())
            {
                Console.WriteLine("Could not stop capture.");
                Console.ReadLine();
                return;
            }



            // Release memory (to Stable state)
            if (!cam.FreeCamBuffer())
            {
                Console.WriteLine("Could not release camera buffer.");
                Console.ReadLine();
                return;
            }


            Console.ReadLine();
        }
    }
}
